import { useState } from "react";
import "./App.css";
// import Main from "./components/main/Main";
import Navbar from "./components/navbar/Navbar";
import Sidebar from "./components/sidebar/Sidebar";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./pages/Home";
import Test from "./pages/Test";
import UserList from "./pages/userList/UserList";
import DepartmentList from "./pages/Department/DepartmentList";

const App = () => {
  const [sidebarOpen, setsidebarOpen] = useState(false);
  const openSidebar = () => {
    setsidebarOpen(true);
  };
  const closeSidebar = () => {
    setsidebarOpen(false);
  };
  return (
    <>
      <Router>
        <div className="container">
          <Navbar sidebarOpen={sidebarOpen} openSidebar={openSidebar} />
          <Sidebar sidebarOpen={sidebarOpen} closeSidebar={closeSidebar} />
          <Switch>
            <Route exact path="/">
              <Home />
            </Route>
            <Route exact path="/test">
              <Test />
            </Route>
            <Route path="/users">
              <UserList />
            </Route>
          </Switch>
          <Route path="/department">
            <DepartmentList />
          </Route>
        </div>
      </Router>
    </>
    // <div className="container">
    //   <Navbar sidebarOpen={sidebarOpen} openSidebar={openSidebar} />
    //   <Sidebar sidebarOpen={sidebarOpen} closeSidebar={closeSidebar} />
    //   <Main />
    // </div>
  );
};

export default App;
