import "./userList.css";
import { DataGrid } from "@material-ui/data-grid";
import { DeleteOutline } from "@material-ui/icons";
import { useState, useEffect } from "react";
import axios from "axios";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import Radio from "@material-ui/core/Radio";
import TextField from "@material-ui/core/TextField";

//Modal
const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function UserList() {
  //Function Modal
  const classes = useStyles();
  const [open, setOpen] = useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  //End Function Modal

  const [data, setData] = useState([]);
  const [department, setDepartment] = useState([]);
  const [modalStatus, setModalStatus] = useState(0);
  const [form, setForm] = useState({
    UserId: 0,
    UserName: "",
    Department: "",
    Email: "",
    Status: "",
    DateOfJoin: "",
    PhotoFileName: "",
  });

  const setNewForm = () => {
    setForm({
      UserId: 0,
      UserName: "",
      Department: "",
      Email: "",
      Status: true,
      DateOfJoin: "",
      PhotoFileName: "",
    });
  };

  useEffect(() => {
    getDataUser();
    getDataDepartment();
  }, []);

  const getDataDepartment = () => {
    axios
      .get("http://localhost:5000/api/user/GetAllDepartmentNames")
      .then(function (response) {
        // handle success
        // console.log(response.data);
        setDepartment(response.data);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  };

  const getDataUser = () => {
    axios
      .get("http://localhost:5000/api/user/")
      .then(function (response) {
        // handle success
        // console.log(response.data);
        setData(response.data);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  };

  const addDataUser = () => {
    if (
      form.UserName !== "" &&
      form.Department !== "" &&
      form.Email !== "" &&
      form.DateOfJoin !== ""
    ) {
      // console.log("form", form);
      axios
        .post("http://localhost:5000/api/user", form)
        .then(function (response) {
          // handle success
          console.log(response.data);
          getDataUser();
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        });
    }
  };

  const editDataUser = () => {
    if (
      form.UserName !== "" &&
      form.Department !== "" &&
      form.Email !== "" &&
      form.DateOfJoin !== ""
    ) {
      axios
        .put("http://localhost:5000/api/user/", form)
        .then(function (response) {
          // handle success
          console.log(response.data);
          getDataUser();
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        });
    }
  };

  const deleteDataUser = (id) => {
    axios
      .delete("http://localhost:5000/api/user/" + id)
      .then(function (response) {
        // handle success
        console.log(response.data);
        getDataUser();
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  };

  function handleSubmit(event) {
    handleClose();
    event.preventDefault();
  }

  const columns = [
    { field: "id", headerName: "ID", width: 60 },
    // {
    //   field: "user",
    //   headerName: "User",
    //   width: 200,
    //   renderCell: (params) => {
    //     return (
    //       <div className="userListUser">
    //         <img className="userListImg" src={params.row.avatar} alt="" />
    //         {params.row.username}
    //       </div>
    //     );
    //   },
    // },
    { field: "UserName", headerName: "UserName", width: 120 },
    {
      field: "Department",
      headerName: "Department",
      width: 150,
    },
    {
      field: "Email",
      headerName: "Email",
      width: 160,
    },
    {
      field: "Status",
      headerName: "Status",
      width: 120,
    },
    {
      field: "DateOfJoin",
      headerName: "DateOfJoin",
      width: 120,
    },
    {
      field: "PhotoFileName",
      headerName: "PhotoFileName",
      width: 120,
    },
    {
      field: "action",
      headerName: "Action",
      width: 150,
      renderCell: (params) => {
        return (
          <>
            <button
              className="userListEdit"
              onClick={() => {
                setForm(params.row);
                setModalStatus(1);
                console.log(params.row);
                handleOpen();
              }}
            >
              Edit
            </button>
            <DeleteOutline
              className="userListDelete"
              onClick={() => deleteDataUser(params.row.id)}
            />
          </>
        );
      },
    },
  ];

  return (
    <div className="userList">
      <div className="userTitleContainer">
        <h1 className="userTitle">User</h1>
        <button
          className="userAddButton"
          onClick={() => {
            handleOpen();
            setNewForm();
            setModalStatus(0);
          }}
        >
          Create
        </button>
      </div>

      <DataGrid
        rows={data}
        disableSelectionOnClick
        columns={columns}
        pageSize={8}
        checkboxSelection
        className="userContainer"
      />
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <form className="newuserForm" onSubmit={handleSubmit}>
              <div className="newuserItem">
                <label>User Name</label>
                <input
                  type="text"
                  placeholder="Username"
                  value={form.UserName}
                  required="required"
                  onChange={(e) =>
                    setForm({ ...form, UserName: e.target.value })
                  }
                />
              </div>
              <div className="newuserItem">
                <label>Department</label>
                <select
                  type="text"
                  placeholder="Department"
                  value={form.Department}
                  required="required"
                  onChange={(e) =>
                    setForm({ ...form, Department: e.target.value })
                  }
                >
                  <option key="0"></option>
                  {department.map((item) => (
                    <option key={item.id}>{item.DepartmentName}</option>
                  ))}
                </select>
              </div>
              <div className="newuserItem">
                <label>Email</label>
                <input
                  type="text"
                  placeholder="name@example.com"
                  value={form.Email}
                  required="required"
                  onChange={(e) => setForm({ ...form, Email: e.target.value })}
                />
              </div>
              <div className="newuserItem">
                <label>Status</label>
                <div>
                  <Radio
                    checked={form.Status === true}
                    onChange={(e) => setForm({ ...form, Status: true })}
                    value={form.Status}
                    required="required"
                    color="default"
                    name="radio-status"
                    size="small"
                  />
                  Yes
                  <Radio
                    checked={form.Status === false}
                    onChange={(e) => setForm({ ...form, Status: false })}
                    value={form.Status}
                    color="default"
                    name="radio-status"
                    size="small"
                  />
                  No
                </div>
              </div>
              <div className="newuserIten">
                <TextField
                  id="date"
                  label="Date Of Join"
                  type="date"
                  // defaultValue="2017-05-24"
                  className={classes.textField}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  value={form.DateOfJoin}
                  required="required"
                  onChange={(e) =>
                    setForm({ ...form, DateOfJoin: e.target.value })
                  }
                />
              </div>
              <div className="newuserItem">
                <button
                  className="newuserButton"
                  onClick={modalStatus === 0 ? addDataUser : editDataUser}
                >
                  {modalStatus === 0 ? "Add" : "Edit"}
                </button>
              </div>
            </form>
          </div>
        </Fade>
      </Modal>
    </div>
  );
}
