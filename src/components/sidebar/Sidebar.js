import "./Sidebar.css";
// import logo from "../../assets/logo.png";
import { Link } from "react-router-dom";

const Sidebar = ({ sidebarOpen, closeSidebar }) => {
  return (
    <div className={sidebarOpen ? "sidebar_responsive" : ""} id="sidebar">
      <div className="sidebar__title">
        <div className="sidebar__img">
          {/* <img src={logo} alt="logo" /> */}
          <h1>Rudi Admin</h1>
        </div>
        <i
          onClick={() => closeSidebar()}
          className="fa fa-times"
          id="sidebarIcon"
          aria-hidden="true"
        ></i>
      </div>

      <div className="sidebar__menu">
        <Link to="/" className="link">
          <div className="sidebar__link">
            <i className="fa fa-home"></i>
            Dashboard
          </div>
        </Link>
        <h2>Mini Class</h2>
        <Link to="/users" className="link">
          <div className="sidebar__link">
            <i className="fa fa-user-secret" aria-hidden="true"></i>
            User
          </div>
        </Link>
        <Link to="/department" className="link">
          <div className="sidebar__link">
            <i className="fa fa-building-o"></i>
            Department
          </div>
        </Link>
        <div className="sidebar__logout">
          <i className="fa fa-power-off"></i>
          <a href="/">Log out</a>
        </div>
      </div>
    </div>
  );
};

export default Sidebar;
