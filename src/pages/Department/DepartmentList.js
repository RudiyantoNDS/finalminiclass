import "./DepartmentList.css";
import { DataGrid } from "@material-ui/data-grid";
import { DeleteOutline } from "@material-ui/icons";
import { useState, useEffect } from "react";
import axios from "axios";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";

//Modal
const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function DepartmentList() {
  //Function Modal
  const classes = useStyles();
  const [open, setOpen] = useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  //End Function Modal

  const [data, setData] = useState([]);
  const [modalStatus, setModalStatus] = useState(0);
  const [form, setForm] = useState({
    DepartmentId: 0,
    DepartmentName: "",
  });

  const setNewForm = () => {
    setForm({
      DepartmentId: 0,
      DepartmentName: "",
    });
  };

  useEffect(() => {
    getDataDepartment();
  }, []);

  const getDataDepartment = () => {
    axios
      .get("http://localhost:5000/api/department/")
      .then(function (response) {
        // handle success
        // console.log(response.data);
        setData(response.data);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  };

  const addDataDepartment = () => {
    if (form.DepartmentName !== "") {
      // console.log("form", form);
      // alert("Department is required");
      axios
        .post("http://localhost:5000/api/department", form)
        .then(function (response) {
          // handle success
          console.log(response.data);
          getDataDepartment();
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        });
    }
  };

  const editDataDepartment = () => {
    if (form.DepartmentName !== "") {
      axios
        .put("http://localhost:5000/api/department/", form)
        .then(function (response) {
          // handle success
          console.log(response.data);
          getDataDepartment();
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        });
    }
  };

  const deleteDataClient = (id) => {
    axios
      .delete("http://localhost:5000/api/department/" + id)
      .then(function (response) {
        // handle success
        console.log(response.data);
        getDataDepartment();
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  };

  function handleSubmit(event) {
    handleClose();
    event.preventDefault();
  }

  const columns = [
    { field: "DepartmentId", headerName: "ID", width: 90 },
    { field: "DepartmentName", headerName: "Department", width: 200 },
    {
      field: "action",
      headerName: "Action",
      width: 150,
      renderCell: (params) => {
        return (
          <>
            <button
              className="departmentListEdit"
              onClick={() => {
                setForm(params.row);
                setModalStatus(1);
                console.log(params.row);
                handleOpen();
              }}
            >
              Edit
            </button>
            <DeleteOutline
              className="departmentListDelete"
              onClick={() => deleteDataClient(params.row.id)}
            />
          </>
        );
      },
    },
  ];

  return (
    <div className="departmentList">
      <div className="departmentTitleContainer">
        <h1 className="departmentTitle">Department</h1>
        <button
          className="departmentAddButton"
          onClick={() => {
            handleOpen();
            setNewForm();
            setModalStatus(0);
          }}
        >
          Create
        </button>
      </div>
      <DataGrid
        rows={data}
        disableSelectionOnClick
        columns={columns}
        pageSize={5}
        checkboxSelection
        className="departmentContainer"
      />
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <form className="newdepartmentForm" onSubmit={handleSubmit}>
              <div className="newdepartmentItem">
                <label>Department Name</label>
                <input
                  type="text"
                  placeholder="Deparment"
                  value={form.DepartmentName}
                  required="required"
                  onChange={(e) =>
                    setForm({ ...form, DepartmentName: e.target.value })
                  }
                />
              </div>
              <div className="newdepartmentItem">
                <button
                  className="newdepartmentButton"
                  onClick={
                    modalStatus === 0 ? addDataDepartment : editDataDepartment
                  }
                >
                  {modalStatus === 0 ? "Add" : "Edit"}
                </button>
              </div>
            </form>
          </div>
        </Fade>
      </Modal>
    </div>
  );
}
